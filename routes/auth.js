const router = require('express').Router();
const User = require('../model/User')
const { registerValidation, loginValidation } = require('../validation')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

router.get('/', async (req, res) => {
    try {
        const users = await User.find();
        res.status(200).json(users)

    } catch (error) {
        res.status(500).json({ message: error.message })
    }
});

router.get('/:id', getUser, (req, res) => {
    res.json(res.user)
});

router.post('/', async (req, res) => {

    //Validation dates
    const { error } = registerValidation(req.body)
    if (error) return res.status(400).send({ message: error.details[0].message })

    //Validation exists email
    const emailExists = await User.findOne({ email: req.body.email })
    if (emailExists) return res.status(400).send("Email already exists")

    // random first part of the hash
    const salt = await bcrypt.genSalt(10);
    //Hash password
    const hashedPassword = await bcrypt.hash(req.body.password, salt);


    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword
    });

    try {
        const newUser = await user.save()
        res.status(201).send({ user: user._id })
    } catch (error) {
        res.status(400).send({ message: error.message })
    }

});


router.post('/login', async (req, res) => {

    //Validation dates
    const { error } = loginValidation(req.body);
    if (error) return res.status(400).send({ message: error.details[0].message })

    //Validation exists email
    const user = await User.findOne({ email: req.body.email })
    if (!user) return res.status(400).send("Email is not found")

    //Validation Password is correctC 
    const validPassword = await bcrypt.compare(req.body.password, user.password)
    if (!validPassword) return res.status(400).send("Invalid password")

    //Create and assing a token 
    const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);
    res.header('auth-token', token).send(token);

    //res.send('Logged in!')

});

router.patch('/:id', getUser, async (req, res) => {
    if (req.body.name != null) {
        res.user.name = req.user.name
    }
    if (req.body.email != null) {
        res.user.email = req.user.email
    }
    if (req.body.password != null) {
        res.user.password = req.user.password
    }

    try {
        const updatedUser = await res.user.save()
        res.json(updatedUser)
    } catch (error) {
        res.status(400).json({ message: error.message })
    }
});

router.delete('/:id', getUser, async (req, res) => {

    try {
        await res.user.remove()
        res.json({ message: "Deleted User" })
    } catch (error) {
        res.status(500).json({ message: error.message })
    }

});

async function getUser(req, res, next) {
    let user
    try {
        user = await User.findById(req.params.id)
        console.log(user)
        if (user == null) { res.status(400).json({ message: "Cannon find User" }) }

    } catch (error) {
        res.status(500).json({ message: error.message })
    }
    res.user = user;
    next()
}


module.exports = router;