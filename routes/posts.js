const router = require('express').Router();
const verify = require('./verifyToken');

//private route with token access
router.get("/", verify, (req, res) => {
    res.json({ posts: { title: "My First post", description: "Random data" } });    

})
module.exports = router;