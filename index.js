const express = require('express');
const app = express();
const dotenv = require('dotenv')
const mongoose = require('mongoose')
//Import Routes
const authRoute = require('./routes/auth');
const postRoute = require('./routes/posts');


dotenv.config()

//Mongo Connection  // In local docker container or clound
mongoose.connect(process.env.DATABASE_URL, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    })
    .then(() => console.log('DB Connected'))
    .catch(err => {
    console.log("DB Connection Error: "+ err.message);
    });

//Routes Midlewares
app.use(express.json())
app.use('/users', authRoute)
app.use('/posts', postRoute)


//Server
app.listen(3000, () => console.log("Server Up and running "))